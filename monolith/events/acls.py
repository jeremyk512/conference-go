import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    data = json.loads(response.text)
    # Get the URL for the first picture in the response
    picture_url = data["photos"][0]["src"]["medium"]
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return picture_url


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geo_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    geo_response = requests.get(geo_url)
    # Parse the JSON response
    geo_data = json.loads(geo_response.text)
    # Get the latitude and longitude from the response
    lat = geo_data[0]["lat"]
    lon = geo_data[0]["lon"]
    # Create the URL for the weather API with the latitude and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    weather_response = requests.get(weather_url)
    # Parse the JSON response
    weather_data = json.loads(weather_response.text)
    # Get the temperature and description from the response
    # and put them in a dictionary
    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    # Return the dictionary
    return {"temp": temp, "description": description}
