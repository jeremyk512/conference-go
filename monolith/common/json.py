from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    """
    This is a JSONEncoder that can be used to encode a datetime into a
    string. The string will be in ISO 8601 format.
    """
    def default(self, o):
        """
        Returns a string in ISO 8601 format if the object is a
        datetime. Otherwise, returns the value returned by the
        default() method of the superclass.
        """
        if isinstance(o, datetime):
            """
            If the object is a datetime, then return a string in ISO
            8601 format.
            """
            return o.isoformat()
        else:
            """
            Otherwise, return the value returned by the default()
            method of the superclass.
            """
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    """
    This is a JSONEncoder that can be used to encode a Django QuerySet
    into a list. The list will contain the model's properties and
    values. If the model has a get_api_url() method, then the
    dictionary will contain an href property with the URL to view the
    model.
    """
    def default(self, o):
        """
        Returns a list with the model's properties and values.
        """
        if isinstance(o, QuerySet):
            """
            If the object is a QuerySet, then return a list with the
            model's properties and values.
            """
            return list(o)
        else:
            """
            Otherwise, return the value returned by the default()
            method of the superclass.
            """
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    """
    This is a JSONEncoder that can be used to encode a Django model
    into a dictionary. The dictionary will contain the model's
    properties and values. If the model has a get_api_url() method,
    then the dictionary will contain an href property with the URL to
    view the model.
    """
    encoders = {}

    def default(self, o):
        """
        Returns a dictionary with the model's properties and values.
        """
        if isinstance(o, self.model):
            """
            If the object has a get_api_url() method, then add the
            href property to the dictionary.
            """
            d = {}
            if hasattr(o, "get_api_url"):
                """
                If the object has a get_api_url() method, then add the
                href property to the dictionary.
                """
                d["href"] = o.get_api_url()
            for property in self.properties:
                """
                Add the property to the dictionary. If the property is
                in the encoders dictionary, then call the encoder
                function on the value before adding it to the
                dictionary."""
                value = getattr(o, property)
                if property in self.encoders:
                    """
                    If the property is in the encoders dictionary, then
                    call the encoder function on the value before
                    adding it to the dictionary.
                    """
                    encoder = self.encoders[property]()
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            """
            Add any extra data to the dictionary.
            """
            return d
        else:
            """
            Otherwise, return the value returned by the default()
            method of the superclass.
            """
            return super().default(o)

    def get_extra_data(self, o):
        """
        Returns a dictionary with any extra data that should be added
        to the dictionary.
        """
        return {}
